/* Author: Romain "Artefact2" Dal Maso <artefact2@gmail.com> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include "common.h"

static int stardict_compare_qsort(const void* x, const void* y) {
	return stardict_compare(*(const char**)x, *(const char**)y);
}

int main(int argc, char** argv) {
	FILE* idxf;
	char* idx;
	long idxlen;

	if(argc != 2) {
		fprintf(stderr, "Usage: %s <idx-file>\n", argv[0]);
		return 1;
	}

	idxf = fopen(argv[1], "rb");
	if(idxf == NULL) {
		perror("cannot open index file");
		return 1;
	}

	if(fseek(idxf, 0, SEEK_END) < 0) {
		perror("fseek failed");
		return 1;
	}

	if((idxlen = ftell(idxf)) < 0) {
		perror("ftell failed");
		return 1;
	}

	idx = mmap(NULL, idxlen, PROT_READ, MAP_SHARED, fileno(idxf), 0);
	if(idx == MAP_FAILED) {
		perror("cannot mmap index file");
		return 1;
	}

	const void** data;
	size_t datalen;

	parse_index(idx, idxlen, &data, &datalen);
	qsort(data, datalen, sizeof(void*), stardict_compare_qsort);

	char tempidxn[strlen(argv[1]) + 5];
	sprintf(tempidxn, "%s.tmp", argv[1]);
	FILE* tempidxf = fopen(tempidxn, "wb");
	if(tempidxf == NULL) {
		perror("cannot open temp index file");
		return 1;
	}

	for(size_t i = 0; i < datalen; ++i) {
		if(!fwrite(data[i], strlen(data[i]) + 9, 1, tempidxf)) {
			perror("cannot write to temp index file");
			return 1;
		}
	}

	/* Can of worms. This minimises but doesn't eliminate risk of
	 * trashing both .idx and .idx.tmp. */
	if(fdatasync(fileno(tempidxf))) {
		perror("fdatasync() failed, you are on your own");
	}

	free(data);
	fclose(tempidxf);
	munmap(idx, idxlen);
	fclose(idxf);

	return rename(tempidxn, argv[1]);
}
